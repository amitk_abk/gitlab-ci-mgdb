# Sample code base to work with Mongo DB and generate report with Gitlab-ci  
A sample spring boot based code base that works with mongo db as data store. The code base has a sample test case that works with mongo db (not embedded).

### The test report is available at https://amitk_abk.gitlab.io/gitlab-ci-mgdb/


## References:
### https://medium.com/turpialdev/how-to-host-an-html-page-in-gitlab-pages-47bbfbdeb4d1
### https://gitlab.com/gitlab-org/gitlab-ci-yml/blob/58ff8bb20dfcb3133932882d7d264fabced2a248/Gradle.gitlab-ci.yml
