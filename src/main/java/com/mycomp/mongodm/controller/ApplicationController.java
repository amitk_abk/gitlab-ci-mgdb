package com.mycomp.mongodm.controller;

import com.mycomp.mongodm.model.User;
import com.mycomp.mongodm.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/controller")
public class ApplicationController {

    @Autowired
    UserService userService;

    @PostMapping("/user")
    public User saveUser() {
        User user = new User();
        long userId = user.getUserId();
        user.setFirstName("User_" + userId);
        user.setLastName("User_" + userId);
        user.setCity("Bangalore");
        user = userService.saveUser(user);
        return user;
    }

    @GetMapping("/user")
    public List<User> getUser() {
        return userService.getAllUsers();
    }
}
