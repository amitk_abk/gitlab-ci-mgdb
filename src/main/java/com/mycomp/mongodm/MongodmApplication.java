package com.mycomp.mongodm;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MongodmApplication {

	public static void main(String[] args) {
		SpringApplication.run(MongodmApplication.class, args);
	}

}
