package com.mycomp.mongodm.service;

import com.mycomp.mongodm.model.User;
import com.mycomp.mongodm.repositories.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class UserService {

    @Autowired
    UserRepository userRepository;

    public User saveUser(User user) {
        user = userRepository.save(user);
        return user;
    }

    public void deleteAll() {
        userRepository.deleteAll();
    }

    public List<User> getAllUsers() {
        return userRepository.findAll();
    }
}
