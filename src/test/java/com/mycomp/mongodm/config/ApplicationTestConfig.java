package com.mycomp.mongodm.config;

import com.mongodb.MongoClient;
import com.mongodb.MongoClientOptions;
import com.mongodb.ServerAddress;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.*;
import org.springframework.data.mongodb.MongoDbFactory;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.SimpleMongoDbFactory;
import org.springframework.data.mongodb.core.convert.DbRefResolver;
import org.springframework.data.mongodb.core.convert.DefaultDbRefResolver;
import org.springframework.data.mongodb.core.convert.DefaultMongoTypeMapper;
import org.springframework.data.mongodb.core.convert.MappingMongoConverter;
import org.springframework.data.mongodb.core.mapping.MongoMappingContext;

@Configuration
@Profile("test")
public class ApplicationTestConfig {

    @Value("${mongodb.host}")
    private String mongoHost;

    @Value("${mongodb.port}")
    private Integer mongoPort;

    @Value("${mongodb.database}")
    private String mongoDatabase;


    @Bean("mongoClientTest")
    @Profile("test")
    @Primary
    public MongoClient mongoClientTest() {
        MongoClientOptions.Builder options = MongoClientOptions.builder();
        options.minConnectionsPerHost(10);
        options.maxConnectionIdleTime(10 * 60 * 1000);
        options.maxConnectionLifeTime(60 * 60 * 1000);
        return new MongoClient(new ServerAddress(mongoHost, mongoPort), options.build());
    }

    @Bean("mongoDbFactoryTest")
    @Profile("test")
    @Primary
    public MongoDbFactory mongoDbFactoryTest() {
        return new SimpleMongoDbFactory(mongoClientTest(), mongoDatabase);
    }

    @Bean
    @Profile("test")
    @Primary
    public MongoTemplate mongoTemplateTest() {
        DbRefResolver dbRefResolver = new DefaultDbRefResolver(mongoDbFactoryTest());
        MappingMongoConverter converter = new MappingMongoConverter(dbRefResolver, new MongoMappingContext());
        converter.setTypeMapper(new DefaultMongoTypeMapper(null));
        return new MongoTemplate(mongoDbFactoryTest(), converter);
    }

}
