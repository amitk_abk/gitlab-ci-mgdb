package com.mycomp.mongodm.controller;

import com.mycomp.mongodm.MongodmApplication;
import com.mycomp.mongodm.config.ApplicationTestConfig;
import com.mycomp.mongodm.model.User;
import com.mycomp.mongodm.service.UserService;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.List;

@ActiveProfiles("test")
@RunWith(SpringRunner.class)
@SpringBootTest(classes = {MongodmApplication.class, ApplicationTestConfig.class})
public class ApplicationControllerTest {

    @Autowired
    UserService userService;

    @Test
    public void saveUserTest() throws Exception {

        userService.deleteAll();

        User user = new User();
        long userId = user.getUserId();
        user.setFirstName("Test User_" + userId);
        user.setLastName("Test User_" + userId);
        user.setCity("Test City");
        user = userService.saveUser(user);

        List<User> userList = userService.getAllUsers();
        Assert.assertEquals(user, userList.get(0));
    }
}
